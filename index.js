var kue = require('kue');
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var which = require('shelljs').which;

var app = express();

var pid = process.pid;

var Hose = function(args){
  if(args) {
    this.args = args;
    this.jobsProcessed = 0;
    this.jobsCreated = 0;
    this.init();
  }
};

Hose.prototype.init = function(){
  var that = this;
  if(!this.args) console.log("flowr-hose:init","No config");
  this.service = this.args.name;
  this.queue = kue.createQueue(this.args.kue);

  if(this.queue) {
    this.queue.process(this.service + ':ping', ping);
  }

  app.get('/', function (req, res) {
    res.json(that.args.flowr || that.args.name);
  });

  app.get('/ping', function (req, res) {
    ping(function(err, data){
      if(err) return res.json(err);
      return res.json(data);
    });
  });

  app.get('/status', function (req, res) {
    return res.status(200).send('OK. Jobs processed: '+ that.jobsProcessed +'. Jobs created: '+ that.jobsCreated);
  });

  if(this.args.ui){
    kue.app.listen(this.args.ui.port || 3000);
    if(this.args.ui.title){
      kue.app.set('title', this.args.ui.title);
    }
  }

  this.http = {};
  this.http.app = app;
  this.http.server = app.listen(this.args.port, function () {
    var host = that.http.server.address().address;
    var port = that.http.server.address().port;
    console.log('FlowR microservice '+ that.service +' running at http://%s:%s', host, port);
  });
};

Hose.prototype.config = function(args){
  this.args = args;
};

Hose.prototype.process = function(job, n, cb){
  var self = this;
  console.log("PROCESSSING /"+job);
  if( 'function' == typeof n ) {
    cb = n;
    n = 1;
  }

  this.queue.process(this.service+':'+job, n, function(job, done){
    cb(job, function(err, result){
      self.jobsProcessed++;
      done(err, result);
    });
  });

  // generate new route
  app.get('/'+job, function(req, res){
    cb({data:req.body, log: console.log }, function(err, output){
      if(err) return res.json(err);
      self.jobsProcessed++;
      res.json(output);
    });
  });
};

Hose.prototype.create = function(name, args, remove, cb){
  var self = this;

  if(typeof remove !== 'boolean') {
    cb = remove;
    remove = false;
  }

  var job = this.queue.create(name, args)
  .removeOnComplete( remove )
	.save( function(err){
	   if( err ) {
      console.log("job.save error: ", err);
       cb(err, null);
     } else {
       self.jobsCreated++;
       console.log("NEW JOB ID: ", job.id );
     }
	}).on('complete', function(result){
		console.log('job completed with data ', result);
    cb(null, result);
	}).on('error', function(err){
		sails.log.error("job ERR:", err);
    cb(err, null);
	});
};

function ping(job, done) {
  if(!done) done = job;
  return done(null, {ping:"pong"});
}

module.exports = Hose;
